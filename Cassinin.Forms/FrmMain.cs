﻿using Cassinin.BuildHost;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Cassinin.Forms {
    public partial class FrmMain : Form {
        Logger Logger;
        TaskScheduler UiScheduler;
        private ProjectHost _server;
        private bool _isServerRun = false;
        public FrmMain() {
            InitializeComponent();
            lblRootUrl.Visible = false;
            linkRootUrl.Visible = false;
            FormClosed += (o, e) => stopServer();
        }
        void stopServer() {
            if (_server != null)
                _server.Dispose();
            _server = null;
        }
        private void FrmMain_Load(object sender, EventArgs e) {
            Logger = LogManager.GetCurrentClassLogger();
            UiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            tbIpAddress.Text = IPAddress.Loopback.ToString();
        }
        private void bRunAndStop_Click(object sender, EventArgs e) {
            if (_isServerRun) Stop();
            else Run();
        }
        void Run() {
            if (!File.Exists(tbProjectFile.Text)) {
                Logger.Log(LogLevel.Error, string.Format("File {0} not found", tbProjectFile.Text));
                return;
            }
            IPAddress ip;
            if (IPAddress.TryParse(tbIpAddress.Text, out ip) == false){
                Logger.Log(LogLevel.Error, string.Format("Incorrect ip address: {0}", tbIpAddress.Text));
                return;
            }
            bSelectProject.Enabled = false;
            bRunAndStop.Enabled = false;
            tbIpAddress.Enabled = false;
            Task.Factory
                .StartNew(() => {
                    _server = new ProjectHost(tbProjectFile.Text, ipAddress:ip);
                    _server.Start();
                })
                .ContinueWith(
                    t => {
                        if (t.Exception != null) 
                            Stop();
                        else {
                            lblRootUrl.Visible = linkRootUrl.Visible = _isServerRun = true;
                            linkRootUrl.Text = _server.WebHost.RootUrl;
                            bRunAndStop.Text = "Stop";
                            bRunAndStop.Enabled = true;
                        }
                    },
                    UiScheduler);
        }
        void Stop() {
            Task.Factory
                .StartNew(stopServer)
                .ContinueWith(
                    t => {
                        lblRootUrl.Visible = linkRootUrl.Visible = _isServerRun = false;
                        bSelectProject.Enabled = true;
                        linkRootUrl.Text = string.Empty;
                        bRunAndStop.Text = "Run";
                        bRunAndStop.Enabled = true;
                        tbIpAddress.Enabled = true;
                    },
                    UiScheduler);
        }
        private void bSelectProject_Click(object sender, EventArgs e) {
            if (dlgSelectProject.ShowDialog() != DialogResult.OK)
                tbProjectFile.Text = string.Empty;
            else 
                tbProjectFile.Text = dlgSelectProject.FileName;
        }
        private void linkRootUrl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            if (linkRootUrl.Visible)
                Process.Start(linkRootUrl.Text);
        }

        private void rtbServerLog_TextChanged(object sender, EventArgs e) {
            rtbServerLog.SelectionStart = rtbServerLog.TextLength;
            rtbServerLog.ScrollToCaret();
        }
    }
}
