﻿namespace Cassinin.Forms {
    partial class FrmMain {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent() {
            this.bSelectProject = new System.Windows.Forms.Button();
            this.tbProjectFile = new System.Windows.Forms.TextBox();
            this.linkRootUrl = new System.Windows.Forms.LinkLabel();
            this.lblRootUrl = new System.Windows.Forms.Label();
            this.bRunAndStop = new System.Windows.Forms.Button();
            this.rtbServerLog = new System.Windows.Forms.RichTextBox();
            this.dlgSelectProject = new System.Windows.Forms.OpenFileDialog();
            this.tbIpAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bSelectProject
            // 
            this.bSelectProject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bSelectProject.Location = new System.Drawing.Point(182, 27);
            this.bSelectProject.Name = "bSelectProject";
            this.bSelectProject.Size = new System.Drawing.Size(156, 23);
            this.bSelectProject.TabIndex = 0;
            this.bSelectProject.Text = "Select project file";
            this.bSelectProject.UseVisualStyleBackColor = true;
            this.bSelectProject.Click += new System.EventHandler(this.bSelectProject_Click);
            // 
            // tbProjectFile
            // 
            this.tbProjectFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProjectFile.Location = new System.Drawing.Point(12, 4);
            this.tbProjectFile.Name = "tbProjectFile";
            this.tbProjectFile.ReadOnly = true;
            this.tbProjectFile.Size = new System.Drawing.Size(326, 20);
            this.tbProjectFile.TabIndex = 1;
            // 
            // linkRootUrl
            // 
            this.linkRootUrl.AutoSize = true;
            this.linkRootUrl.Location = new System.Drawing.Point(65, 99);
            this.linkRootUrl.Name = "linkRootUrl";
            this.linkRootUrl.Size = new System.Drawing.Size(38, 13);
            this.linkRootUrl.TabIndex = 2;
            this.linkRootUrl.TabStop = true;
            this.linkRootUrl.Text = "http://";
            this.linkRootUrl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRootUrl_LinkClicked);
            // 
            // lblRootUrl
            // 
            this.lblRootUrl.AutoSize = true;
            this.lblRootUrl.Location = new System.Drawing.Point(12, 99);
            this.lblRootUrl.Name = "lblRootUrl";
            this.lblRootUrl.Size = new System.Drawing.Size(47, 13);
            this.lblRootUrl.TabIndex = 3;
            this.lblRootUrl.Text = "Root url:";
            // 
            // bRunAndStop
            // 
            this.bRunAndStop.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bRunAndStop.Location = new System.Drawing.Point(138, 354);
            this.bRunAndStop.Name = "bRunAndStop";
            this.bRunAndStop.Size = new System.Drawing.Size(75, 23);
            this.bRunAndStop.TabIndex = 4;
            this.bRunAndStop.Text = "Run";
            this.bRunAndStop.UseVisualStyleBackColor = true;
            this.bRunAndStop.Click += new System.EventHandler(this.bRunAndStop_Click);
            // 
            // rtbServerLog
            // 
            this.rtbServerLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbServerLog.Location = new System.Drawing.Point(12, 115);
            this.rtbServerLog.Name = "rtbServerLog";
            this.rtbServerLog.ReadOnly = true;
            this.rtbServerLog.Size = new System.Drawing.Size(326, 233);
            this.rtbServerLog.TabIndex = 5;
            this.rtbServerLog.Text = "";
            this.rtbServerLog.TextChanged += new System.EventHandler(this.rtbServerLog_TextChanged);
            // 
            // dlgSelectProject
            // 
            this.dlgSelectProject.Filter = "All Files (*.*proj)|*.*proj";
            // 
            // tbIpAddress
            // 
            this.tbIpAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbIpAddress.Location = new System.Drawing.Point(75, 64);
            this.tbIpAddress.Name = "tbIpAddress";
            this.tbIpAddress.Size = new System.Drawing.Size(263, 20);
            this.tbIpAddress.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Ip Address";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 389);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbIpAddress);
            this.Controls.Add(this.rtbServerLog);
            this.Controls.Add(this.bRunAndStop);
            this.Controls.Add(this.lblRootUrl);
            this.Controls.Add(this.linkRootUrl);
            this.Controls.Add(this.tbProjectFile);
            this.Controls.Add(this.bSelectProject);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cassinin Server";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bSelectProject;
        private System.Windows.Forms.TextBox tbProjectFile;
        private System.Windows.Forms.LinkLabel linkRootUrl;
        private System.Windows.Forms.Label lblRootUrl;
        private System.Windows.Forms.Button bRunAndStop;
        private System.Windows.Forms.RichTextBox rtbServerLog;
        private System.Windows.Forms.OpenFileDialog dlgSelectProject;
        private System.Windows.Forms.TextBox tbIpAddress;
        private System.Windows.Forms.Label label1;
    }
}

