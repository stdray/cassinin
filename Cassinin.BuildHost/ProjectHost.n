﻿using Cassinin.Core;

using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;

using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using NLog;

using System;
using System.Collections.Generic;
using System.Console;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Timers;


namespace Cassinin.BuildHost {
    public class ProjectHost : IDisposable {
        static Logger              : Logger     = LogManager.GetCurrentClassLogger();
        public ProjectFile         : string;
        public ProjectPath         : string;
        public WebHost             : Server;
        public Port                : int;
        fsWather                   : MultiWatcher;
        timer                      : Timer;
        public this(projectFile : string, fileExtension : string = "", rebuildDelay : uint = 0, ipAddress : IPAddress = null) {
            def deflt   = Settings.Default;
            def exts    = if(string.IsNullOrEmpty <| fileExtension) deflt.FileExtensions else fileExtension;
            def delay   = if(rebuildDelay > 0) rebuildDelay else deflt.RebuildDelay;
            ProjectFile = projectFile;
            ProjectPath = Path.GetDirectoryName <| projectFile;
            timer       = Timer(delay) <- {
                AutoReset = false;
                Enabled   = false;
                Elapsed   += _ => buildAndRun();
            }
            def ip   = if(ipAddress == null) IPAddress.Loopback else ipAddress;
            WebHost  = Server(ProjectPath, ip);
            Port     = WebHost.Port;
            fsWather = MultiWatcher(ProjectPath, exts) <- { Changed += onFileChanged };
        }
        onFileChanged(_ : object, e : ChangeEventArgs) : void {
            Logger.Info(e);
            lock(timer) {
                when(timer.Enabled) 
                    timer.Stop();
                timer.Start();
            }
        }
        getServerAssemblyInfo() : string * string {
            def serverAsm = typeof(Server).Assembly;
            (serverAsm.Location, Path.Combine(ProjectPath, "bin", serverAsm.Location |> Path.GetFileName))
        }
        public Start() : void {
            try{
                build();
                WebHost.Start();
                Logger.Info($"Web server is running at $(WebHost.RootUrl)");
                fsWather.Start();
            }
            catch{
                | ex => Logger.ErrorException("Build fail. Server does not started", ex);
                        throw;
            }
        }
        addServerAssembly() : void{
            def (src, dest) = getServerAssemblyInfo();
            when(File.Exists <| dest)
                File.Delete <| dest;
            File.Copy(src, dest);
        }
        buildAndRun() : void {
            fsWather.Stop();
            _ = Task.Factory.StartNew(() => lock(timer) {
                timer.Stop();
                try {
                    build();
                    Logger.Info($"Web server is running at $(WebHost.RootUrl)" );
                }
                catch {
                    e => Logger.ErrorException($"Build fail. Nothing realoaded. $(e.Message)", e); 
                         throw;
                }
                finally {
                    fsWather.Start();
                }
            });
        }
        build() : void {
            using(projs  = ProjectCollection()) {
                Logger.Info("========BUILD START========");
                def props   = Dictionary() <- [ "Configuration" = "Debug", "Platform" = "AnyCPU" ];
                def logger  = BuildLogger <| Microsoft.Build.Framework.LoggerVerbosity.Minimal;
                def parms   = BuildParameters(projs) <- { Loggers = [ logger ] };
                def request = BuildRequestData(ProjectFile, props, null, array[ "Build" ], null);
                def result  = BuildManager.DefaultBuildManager.Build(parms, request);
                when(result.Exception != null)
                    throw result.Exception;
                def fails   = result.ResultsByTarget.Where(r => r.Value.ResultCode == TargetResultCode.Failure);
                Logger.Info($"Errors: $(logger.ErrorsCount); Warnings: $(logger.WarningsCount)");
                Logger.Info("=========BUILD END=========");
                when(fails.Any()) {
                    def errs = fails.Select(f => f.Key).NToList();
                    def msg  = $"Build targets fail: $errs";
                    throw InvalidOperationException(msg);
                }
            }
        }
        public Dispose() : void{ 
            fsWather.Dispose();
            WebHost.Dispose();
            timer.Dispose();
            def (_, dest) = getServerAssemblyInfo();
            when(File.Exists <| dest)
                File.Delete <| dest;
        }
    }
}
