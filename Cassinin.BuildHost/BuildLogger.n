﻿using Microsoft.Build.Framework;
using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cassinin.BuildHost {
    class BuildLogger : ILogger {
        static Logger         : NLog.Logger = NLog.LogManager.GetCurrentClassLogger();
        public LogVerbosity   : LoggerVerbosity;
        public ErrorsCount    : int { get; private set }
        public WarningsCount  : int { get; private set }
        public this(verbosity : LoggerVerbosity){
            LogVerbosity  = verbosity;
            ErrorsCount   = 0;
            WarningsCount = 0;
        }
        public Initialize(eventSource : IEventSource) : void {
            _ = eventSource <- {
                ErrorRaised        += OnError;
                    //MessageRaised    += (o, e) => OnSome(o,e);
                WarningRaised      += OnWarning;
                    //BuildFinished      += OnFinish;
                    //StatusEventRaised  += OnStatus;
                }
        }
        OnError(_ : object, e : BuildErrorEventArgs) : void {
            ErrorsCount++;
            logFile("Error", e.File, e.LineNumber, e.ColumnNumber, e.Message)
        }
        OnWarning(_ : object, e : BuildWarningEventArgs) : void {
            WarningsCount++;
            logFile("Warning", e.File, e.LineNumber, e.ColumnNumber, e.Message)
        }
        #pragma warning disable 10003
        OnFinish(_ : object, e : BuildFinishedEventArgs) : void {
            Logger.Info($"$(e.Message)");
        }
        OnStatus(_ : object, e : BuildStatusEventArgs) : void {
            when(e is ProjectStartedEventArgs || e is ProjectFinishedEventArgs)
                Logger.Info($"$(e.Message)");
        }
        #pragma warning restore 10003
        logFile(evnt : string, file : string, line : int, column : int, message : string): void {
            Logger.Info($"$evnt $file $line/$column : $message");
        }
        public Parameters : string {  get; set; }
        public Shutdown() : void{  }
        public Verbosity : LoggerVerbosity {
            get { LogVerbosity  }
            set { _ = value; }
        }

    }
}
