﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Cassinin.BuildHost {
    public class MultiWatcher : IDisposable {
        public Path           : string;
        watcher               : FileSystemWatcher;
        extensions            : HashSet[string];
        public event Changed  : EventHandler[ChangeEventArgs];
        dedublicatePeriod     : TimeSpan        = TimeSpan.FromMilliseconds(100);
        mutable lastEvent     : ChangeEventArgs = ChangeEventArgs(ChangeType.Created, "-1");
        mutable lastEventTime : DateTime        = DateTime.MinValue;
        public OnChange(o : object, e : ChangeEventArgs) : void {
            when(Changed != null)
                Changed.Invoke(o, e)
        }
        public this(path : string, exts : string) {
            Path       = path;
            extensions = HashSet <| exts
                .Split(array[',', ' '], StringSplitOptions.RemoveEmptyEntries)
                .Map('.' + _);
            watcher    = FileSystemWatcher() <- {
                Path = path;
                NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | 
                               NotifyFilters.DirectoryName  | NotifyFilters.Size;
                Filter = "*.*";
                IncludeSubdirectories = true;
                Created += filterAndRaise(ChangeType.Created, _, _);
                Deleted += filterAndRaise(ChangeType.Deleted, _, _);
                Changed += filterAndRaise(ChangeType.Changed, _, _);
                Renamed += filterAndRaise(ChangeType.Renamed, _, _);
                EnableRaisingEvents = false;
            };
        }
        filterAndRaise(type : ChangeType, _ : object, e : FileSystemEventArgs) : void {
            when(Changed != null) {
                def ext = Path.GetExtension(e.FullPath).ToLower();
                when(ext |> extensions.Contains)
                    lock(lastEvent) {
                        def relPath     = e.FullPath.Replace(Path, string.Empty);
                        def changeEvent = ChangeEventArgs(type, relPath);
                        when(changeEvent != lastEvent || DateTime.Now - lastEventTime > dedublicatePeriod) {
                            lastEvent     = changeEvent;
                            lastEventTime = DateTime.Now;
                            Changed.Invoke(this, changeEvent);
                        }
                    }
            }
        }
        public Start() : void {
            watcher.EnableRaisingEvents = true;
        }
        public Stop() : void {
            watcher.EnableRaisingEvents = false;
        }
        public Dispose() : void{
            watcher.Dispose();
        }
    }
}
