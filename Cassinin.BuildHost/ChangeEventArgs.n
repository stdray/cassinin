﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Cassinin.BuildHost {
    public enum ChangeType { | Created | Deleted |  Changed | Renamed }

    [Record, StructuralEquality]
    public class ChangeEventArgs : EventArgs {
        public Type       : ChangeType;
        public Path       : string;
        public override ToString() : string {
            $"$(this.Type): $Path"; 
        }
    }
}
