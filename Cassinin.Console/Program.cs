﻿using Cassinin.BuildHost;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CommandLine;

namespace Cassinin.Console {
    class Program {
        static void Main(string[] args) {
            try {
                var tmp = new Options();
                CommandLine.Parser.Default.ParseArguments(args, tmp);
                var options = buildOptions(tmp);
                using (var host = new ProjectHost(options.ProjectFile, options.FileExtensions, options.RebuildDelay)) {
                    host.Start();
                    System.Console.ReadLine();
                }
            }
            catch (Exception ex) {
                System.Console.WriteLine("Error: {0}", ex.Message);
                System.Console.ReadLine();
            }
        }
        static Options buildOptions(Options ops) {
            var curDir = Directory.GetCurrentDirectory();
            var projs = Directory
                .EnumerateFiles(curDir)
                .Where(f => Path.GetExtension(f).Trim().ToLower().EndsWith("proj"));
            var curDirProj = string.IsNullOrEmpty(ops.ProjectFile)
                ? string.Empty
                : Path.Combine(curDir, Path.GetFileName(ops.ProjectFile));
            var candidates =
                new[] { ops.ProjectFile, curDirProj }
                .Concat(projs)
                .Where(p => !string.IsNullOrEmpty(p));
            var prjFile = candidates.FirstOrDefault(File.Exists);
            if (string.IsNullOrEmpty(prjFile))
                throw new ArgumentException(
                    "Project file not found. Candidates : {0}",
                    string.Join(", ", candidates));
            return new Options {
                ProjectFile = prjFile,
                FileExtensions = ops.FileExtensions,
                RebuildDelay = ops.RebuildDelay,
            };
        }
    }

    class Options {
        [Option('p', "prj", HelpText = "Path to the web application project file")]
        public string ProjectFile { get; set; }
        [Option("exts", HelpText = "File extensions that are necessary to monitor")]
        public string FileExtensions { get; set; }
        [Option('d', "delay", DefaultValue = 0u, HelpText = "Delay interval before rebuild")]
        public uint RebuildDelay { get; set; }
    }
}
