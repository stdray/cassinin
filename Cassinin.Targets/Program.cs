﻿using CommandLine;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;

namespace Cassinin.Targets {
    class Program {
        static void Main(string[] args) {
            try {
                var options = new Options();
                if(Parser.Default.ParseArguments(args, options)) {
                    if (options.EnablePackageRestore)
                        EnablePackageRestore();
                    if (options.InstallTargets)
                        InstallTargets();
                    if (options.RegisterPath)
                        RegisterPath(true);
                    if (options.UnRegisterPath)
                        RegisterPath(false);
                }
            }
            catch(Exception ex) {
                Console.WriteLine("Error: {0}", ex.Message);
            }
            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }
        static void InstallTargets() {
            var targetsDir = AppDomain.CurrentDomain.BaseDirectory;
            var targetsFile = "targets.zip";
            var targetsPath = Path.Combine(targetsDir, targetsFile);
            if (!File.Exists(targetsPath))
                throw new FileNotFoundException(
                    string.Format("Targets file {0} not found in {1}", targetsFile, targetsDir),
                    targetsFile);
            var folders = new[] { Environment.SpecialFolder.ProgramFilesX86, Environment.SpecialFolder.ProgramFiles };
            var prFilesPath = folders
                .Select(Environment.GetFolderPath)
                .Where(p => !string.IsNullOrEmpty(p))
                .First();
            var msBuildPath = Path.Combine(prFilesPath, "MSBuild");
            if (!Directory.Exists(msBuildPath))
                Directory.CreateDirectory(msBuildPath);
            using (var targetsZip = ZipFile.Read(targetsPath))
                targetsZip.ExtractAll(msBuildPath, ExtractExistingFileAction.DoNotOverwrite);
            Console.WriteLine("Targets installed");
        }
        const string NugetVar = "EnableNuGetPackageRestore";
        static void EnablePackageRestore() {
            var value = "true";
            Environment.SetEnvironmentVariable(NugetVar, value, EnvironmentVariableTarget.Machine);
            Console.WriteLine("Nuget package restore enabled");
        }
        const string PathVar = "Path";
        static void RegisterPath(bool toRegister) {
            Func<string, string, bool> pathEq = (p1, p2) => Path.GetDirectoryName(p1) == Path.GetDirectoryName(p2);
            var appDir = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            var current = Environment.GetEnvironmentVariable(PathVar, EnvironmentVariableTarget.Machine);
            current = current ?? "";
            var oldPaths = current.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            var filteredPaths = oldPaths.Where(p => !pathEq(p, appDir));
            Action register = () => {
                var newPaths = filteredPaths.Concat(new[] { appDir });
                var pathsStr = string.Join(";", newPaths);
                Environment.SetEnvironmentVariable(PathVar, pathsStr, EnvironmentVariableTarget.Machine);
                Console.WriteLine("Cassinin Server registered");
            };
            Action unregister = () => {
                var pathsStr = string.Join(";", filteredPaths);
                Environment.SetEnvironmentVariable(PathVar, pathsStr, EnvironmentVariableTarget.Machine);
                Console.WriteLine("Cassinin Server unregistered");
            };
            if (toRegister) register(); else unregister();
        }
    }

    public class Options {
        [Option("installtargets", HelpText = "Install MsBuild tergets")]
        public bool InstallTargets { get; set; }
        [Option("packrestore", HelpText = "Enable nuget package restore")]
        public bool EnablePackageRestore { get; set; }
        [Option("regpath", HelpText = "Register Cassinin in system environment path")]
        public bool RegisterPath { get; set; }
        [Option("unregpath", HelpText = "Remov Cassinin from system environment path")]
        public bool UnRegisterPath { get; set; }
    }
}
